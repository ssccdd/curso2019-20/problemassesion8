/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.NUM_SENSORES;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.aleatorio;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class Promotor implements Runnable {
    private final String iD;
    private final ExecutorService ejecucion;
    private final BlockingQueue<Sensor> almacen;
    private final BlockingQueue<List<Casa>> listaCasas;
    private final List<Future<?>> listaTareas;
    
    // Variable de clase
    private static int idInstalador = 0;

    public Promotor(String iD, ExecutorService ejecucion, BlockingQueue<Sensor> almacen, 
                               BlockingQueue<List<Casa>> listaCasas, List<Future<?>> listaTareas) {
        this.iD = iD;
        this.ejecucion = ejecucion;
        this.almacen = almacen;
        this.listaCasas = listaCasas;
        this.listaTareas = listaTareas;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " prepara una instalación");
        
        try {
            crearInstalador();
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " se ha CANCELADO");
        }
    }
    
    private void crearInstalador() throws InterruptedException {
        Future<?> tarea;
        int numSensores = aleatorio.nextInt(VARIACION) + NUM_SENSORES;
        int id = Promotor.getIdInslatador();
        
        // Se crea el instalador y se para su ejecución
        Instalador instalador = new Instalador("Instalador-"+id,numSensores,almacen,
                                               listaCasas);
        tarea = ejecucion.submit(instalador);
        
        // Añadimos la tarea para su posible cancelación
        listaTareas.add(tarea);
    }

    public String getiD() {
        return iD;
    }
    
    private static synchronized int getIdInslatador() {
        return ++idInstalador;
    }
}
