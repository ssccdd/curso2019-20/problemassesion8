/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo1;

import es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.TipoComponente;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class CrearComponente implements Runnable {
    private final String iD;
    private final ExecutorService fabricacion;
    private final BlockingQueue<Componente>[] almacen;
    private final TipoComponente tipoComponente;
    private final List<Future<?>> listaTareas;
    
    // Variable de clase
    private static int numSerie = 0;

    public CrearComponente(String iD, ExecutorService fabricacion, BlockingQueue<Componente>[] almacen, 
                                      TipoComponente tipoComponente, List<Future<?>> listaTareas) {
        this.iD = iD;
        this.fabricacion = fabricacion;
        this.almacen = almacen;
        this.tipoComponente = tipoComponente;
        this.listaTareas = listaTareas;
    }

    @Override
    public void run() {
        int idComponente = CrearComponente.getNumSerie();
        System.out.println("TAREA-" + iD + " fabrica la " + idComponente + "º pieza");
        
        try {
            
            fabricar(idComponente);
            
        } catch ( InterruptedException ex ) {
            System.out.println("TAREA-" + iD + " CANCELA la fabricación");
        }
            
    }
    
    private void fabricar(int idComponente) throws InterruptedException {
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        Fabricante fabricante = new Fabricante("Fabricante("+idComponente+")", 
                                                tipoComponente, almacen);
        
        Future<?> tarea = fabricacion.submit(fabricante);
        listaTareas.add(tarea);
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Identificador único para crear el componente
     * @return el número de idComponente del componente
     */
    private static synchronized int getNumSerie() {
        return ++numSerie;
    }
}
