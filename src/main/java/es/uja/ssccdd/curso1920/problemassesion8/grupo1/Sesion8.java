/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.COMPONENTES;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.CREAR_COMPONENTE;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.ESPERA_FINALIZACION;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.SISTEMA;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.STOCK;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.TIEMPO_ESPERA;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.TRABAJADORES;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.PROVEEDORES;
import es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.TipoComponente;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author pedroj
 */
public class Sesion8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService ejecucionSistema;
        ExecutorService ejecucion;
        BlockingQueue<Componente>[] almacen;
        BlockingQueue<Ordenador> pedido;
        CountDownLatch esperaFinalizacion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucionSistema = Executors.newScheduledThreadPool(SISTEMA);
        ejecucion = Executors.newFixedThreadPool(TRABAJADORES);
        esperaFinalizacion = new CountDownLatch(ESPERA_FINALIZACION);
        listaTareas = new ArrayList();
        
        // Se inicializa el almacén para los componentes y el pedido final
        almacen = new BlockingQueue[COMPONENTES.length];
        for(int i = 0; i < almacen.length; i++)
            almacen[i] = new LinkedBlockingDeque(STOCK);
        pedido = new LinkedBlockingDeque();
        
        // Se añaden las tareas para crear componentes
        for( TipoComponente tipo : COMPONENTES ) {
            CrearComponente fabrica = new CrearComponente("Fabrica-"+tipo,ejecucion,
                                                          almacen, tipo, listaTareas);
            tarea = ejecucionSistema.scheduleAtFixedRate(fabrica, INICIO, CREAR_COMPONENTE, 
                                                         TimeUnit.SECONDS);
            listaTareas.add(tarea); // Se añade para la cancelación
        }
        
        // Se añaden las tareas de los proveedores para crear el pedido de ordenadores
        for( int i = 0; i < PROVEEDORES; i++ ) {
            Proveedor proveedor = new Proveedor("Proveedor-"+i,almacen,pedido);
            tarea = ejecucionSistema.submit(proveedor);
            listaTareas.add(tarea); // Se añade para la cancelación
        }
        
        // Se crea la tarea de cancelación que se ejecutará pasado el TIEMPO_ESPERA
        // y finalizará las tareas que estén activas
        TareaFinalizacion fin = new TareaFinalizacion(listaTareas,esperaFinalizacion);
        ejecucionSistema.schedule(fin, TIEMPO_ESPERA, TimeUnit.MINUTES);
        System.out.println("HILO(Principal) Espera el tiempo establecido");
        esperaFinalizacion.await();
        
        // Cerramos los marcos de ejecucion
        ejecucionSistema.shutdown();
        ejecucion.shutdown();
        
        // Presentamos el resultado del pedido
        System.out.println("HILO(Principal) el pedido obtenido es \n______________");
        for( Ordenador ordenador : pedido )
            System.out.println(ordenador.toString());
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
