/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo4;

import es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.TipoCasa;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author pedroj
 */
public class Instalador implements Runnable {
    private final String iD;
    private final int numSensores;
    private final BlockingQueue<Sensor> listaSensores;
    private final BlockingQueue<List<Casa>> listaCasas;
    private final List<Casa> instalacion;
    
    // Variable de clase
    private static int numCasa = 0;

    public Instalador(String iD, int numSensores, BlockingQueue<Sensor> listaSensores, 
                                                  BlockingQueue<List<Casa>> listaCasas) {
        this.iD = iD;
        this.numSensores = numSensores;
        this.listaSensores = listaSensores;
        this.listaCasas = listaCasas;
        this.instalacion = new ArrayList();
    }
    
    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Empieza la instalación de " + numSensores +
                           " sensores");
        
        try {
            for( int i = 0; i < numSensores; i++ )
                instalarSensor();
            
            finalizacion();
            System.out.println("TAREA-" + iD + " Finaliza la instalación de " + numSensores +
                           " sensores");
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " se ha CANCELADO");
        } 
    }  
    
    private void instalarSensor() throws InterruptedException {
        Iterator it = instalacion.iterator();
        Casa casa;
        boolean asignado = false;
        
        // Recuperamos el primer sensor
        Sensor sensor = listaSensores.take();
        
        // Asignamos el sensor a una casa
        while ( it.hasNext() && !asignado ) {
            casa = (Casa) it.next();
            asignado = casa.addSensor(sensor);
        }
        
        // Si no se ha podido asignar el sensor se prepara una nueva
        // casa aleatorio
        if( !asignado ) {
            casa = new Casa(Instalador.getNumCasa(), TipoCasa.getCasa());
            casa.addSensor(sensor);
            instalacion.add(casa);
        }
    }
    
    public void finalizacion() {
        try {
            // Se almacenan las casas que ha instalado
            listaCasas.put(instalacion);
        } catch (InterruptedException ex) {
            // No hacemos nada al estar finalizando
        }
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Se crea un número de casa único de forma segura
     * @return número de casa
     */
    public static synchronized int getNumCasa() {
        return ++numCasa;
    }
}
