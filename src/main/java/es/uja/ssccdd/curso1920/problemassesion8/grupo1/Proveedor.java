/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.COMPONENTES;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final String iD;
    private final BlockingQueue<Componente>[] almacen;
    private final BlockingQueue<Ordenador> pedido;
    
    // Variable de clase
    private static int numSerie = 0;

    public Proveedor(String iD, BlockingQueue<Componente>[] almacen, BlockingQueue<Ordenador> pedido) {
        this.iD = iD;
        this.almacen = almacen;
        this.pedido = pedido;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " empieza la preparación del pedido de ordenadores");
        
        try {
            
            while( true )
                prepararPedido();
            
        } catch ( InterruptedException ex ) {
            System.out.println("TAREA-" + iD + " finaliza el pedido de ordenadores");
        } catch (ExecutionException ex) {
            System.out.println("TAREA-" + iD + " ERROR en su ejecución");
        }
    }
    
    private void prepararPedido() throws InterruptedException, ExecutionException {
        int idOrdenador = Proveedor.getNumSerie();
        Ordenador ordenador = new Ordenador();
        ordenador.setiD("Ordenador("+idOrdenador+")");
        
        // Obtenemos los componentes del almacén 
        for( int i = 0; i < COMPONENTES.length; i++ ) {
            // Se bloque hasta obtener el componente
            Componente componente = almacen[i].take();
            
            // Añadimos el componente al ordenador
            ordenador.addComponente(componente);
        }
        
        // Simulamos el tiempo de montaje del ordenador
        TimeUnit.SECONDS.sleep(ordenador.tiempoMontaje());
        
        System.out.println("TAREA-" + iD + " ha finalizado el montaje del ordenador " +
                            ordenador.getiD());
        pedido.put(ordenador);
    }
    
    public String getiD() {
        return iD;
    }
    
    /**
     * Identificador único para crear el ordenador
     * @return el número de idOrdenador
     */
    private static synchronized int getNumSerie() {
        return ++numSerie;
    }
}
