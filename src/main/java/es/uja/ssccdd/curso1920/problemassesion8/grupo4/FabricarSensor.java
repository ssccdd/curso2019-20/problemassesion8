/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo4;

import es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.TipoSensor;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author pedroj
 */
public class FabricarSensor implements Runnable {
    private final String iD;
    private final BlockingQueue<Sensor> almacen;
   
    // Variable de clase
    private static int idSensor = 0;

    public FabricarSensor(String iD, BlockingQueue<Sensor> almacen) {
        this.iD = iD;
        this.almacen = almacen;
    }

    @Override
    public void run() {
        try {
            crearSensor();
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " se ha CANCELADO");
        }
    }
    
    private void crearSensor() throws InterruptedException {
        TipoSensor tipo = TipoSensor.getSensor();
        int id = FabricarSensor.getIdSensor();
        
        Sensor sensor = new Sensor(id,tipo);
        
        // Los añadimos al almacen
        almacen.put(sensor);
        System.out.println("TAREA-" + iD + " se crea el " + sensor);
    }
    
    public String getiD() {
        return iD;
    }
    
    /**
     * Simula un número de serie para el sensor
     * @return número de serie del sensor
     */
    public static synchronized int getIdSensor() {
        return ++idSensor;
    }
}
