/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.ESPERA_FINALIZACION;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.ESPERA_INSTALADOR;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.FABRICACION;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.FABRICANTES;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.INSTALADORES;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.SISTEMA;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo4.Constantes.TIEMPO_ESPERA;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService ejecucionSistema;
        ExecutorService ejecucion;
        BlockingQueue<Sensor> almacen;
        BlockingQueue<List<Casa>> listaCasas;
        CountDownLatch esperaFinalizacion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucionSistema = Executors.newScheduledThreadPool(SISTEMA);
        ejecucion = Executors.newFixedThreadPool(INSTALADORES);
        esperaFinalizacion = new CountDownLatch(ESPERA_FINALIZACION);
        listaTareas = new ArrayList();
        almacen = new DelayQueue();
        listaCasas = new LinkedBlockingDeque();
        
        // Se añaden los fabricantes para los sensores
        for(int i = 0; i < FABRICANTES; i++) {
            FabricarSensor fabricante = new FabricarSensor("Fabricante-"+i, almacen);
            tarea = ejecucionSistema.scheduleAtFixedRate(fabricante, INICIO, 
                                                         FABRICACION, TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        }
        
        // Se añade la tarea del promotor que presenta las casas de cada etapa
        Promotor promotor = new Promotor("Promotor", ejecucion, almacen, listaCasas,
                                         listaTareas);
        tarea = ejecucionSistema.scheduleAtFixedRate(promotor, INICIO, 
                                                        ESPERA_INSTALADOR, TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se crea la tarea de cancelación que se ejecutará pasado el TIEMPO_ESPERA
        // y finalizará las tareas que estén activas
        TareaFinalizacion fin = new TareaFinalizacion(listaTareas,esperaFinalizacion);
        ejecucionSistema.schedule(fin, TIEMPO_ESPERA, TimeUnit.MINUTES);
        System.out.println("HILO(Principal) Espera el tiempo establecido");
        esperaFinalizacion.await();
        
        // Cerramos los marcos de ejecucion
        ejecucionSistema.shutdown();
        ejecucion.shutdown();
        
        // Presentamos el resultado del pedido
        System.out.println("HILO(Principal) el pedido obtenido es \n______________");
        for( int i = 0; i < listaCasas.size(); i++ ) {
            System.out.println("________ INSTALADOR " + (i+1) + " ________");
            List<Casa> instaladas = listaCasas.take();
            for( Casa casa : instaladas )
                System.out.println(casa.toString());
            System.out.println("________ FIN CASAS INSTALADOR ________");
        }
            
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
