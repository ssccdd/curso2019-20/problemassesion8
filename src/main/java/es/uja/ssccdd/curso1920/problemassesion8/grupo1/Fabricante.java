/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo1;

import es.uja.ssccdd.curso1920.problemassesion8.grupo1.Constantes.TipoComponente;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Fabricante implements Runnable {
    private final String iD;
    private final TipoComponente tipoComponente;
    private final BlockingQueue<Componente>[] almacen;

    public Fabricante(String iD, TipoComponente tipoComponente, BlockingQueue<Componente>[] almacen) {
        this.iD = iD;
        this.tipoComponente = tipoComponente;
        this.almacen = almacen;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Fabrica su componente " + tipoComponente);
        
        try {
            prepararComponente();
            
            System.out.println("TAREA-" + iD + " Finaliza el componente " + tipoComponente);
        } catch ( InterruptedException e ) {
            System.out.println("TAREA-" + iD + " Se ha cancelado la fabricación");
        }
    }
    
    private void prepararComponente() throws InterruptedException {
        // Fabrica un nuevo componente
        Componente componente = new Componente(iD+"-"+tipoComponente.ordinal(), tipoComponente);
        
        // Simula la fabricación del componente
        TimeUnit.SECONDS.sleep(componente.tiempoFabricacion());
  
        // Almacenamos el componente en el almacén
        almacen[tipoComponente.ordinal()].put(componente);
    }

    public String getiD() {
        return iD;
    }
}
