/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.EstadoEjecucion.CANCELADO;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.EstadoEjecucion.EN_EJECUCION;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.EstadoEjecucion.FINALIZADO;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.EstadoEjecucion.LISTO;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class HiloEjecucion implements Comparable<HiloEjecucion>, Runnable {
    private final String iD;
    private final Proceso proceso;
    private final BlockingQueue<Proceso> listaFinalizados;

    public HiloEjecucion(String iD, Proceso proceso, BlockingQueue<Proceso> listaFinalizados) {
        this.iD = iD;
        this.proceso = proceso;
        this.proceso.setEstado(LISTO);
        this.listaFinalizados = listaFinalizados;
    }

    @Override
    public void run() {
        try {
            ejecucionProceso();
        } catch ( InterruptedException e ) {
            cancelacionProceso();
        }
    }
    
    /**
     * Simula la ejecución de un proceso por parte de un núcleo del procesador
     * @throws InterruptedException 
     */
    private void ejecucionProceso() throws InterruptedException {
        proceso.setEstado(EN_EJECUCION);
        System.out.println("TAREA-" + iD + " Inicio ejecución " + proceso);
        
        // Simulamos la ejecución del proceso
        TimeUnit.SECONDS.sleep(proceso.tiempoEjecucion());
        proceso.setEstado(FINALIZADO);
        listaFinalizados.put(proceso);
        
        System.out.println("TAREA-" + iD + " Fin ejecución " + proceso);
    }
    
    /**
     * Se cancela la ejecución del proceso en el núcleo del procesador
     */
    private void cancelacionProceso() {
        proceso.setEstado(CANCELADO);
        System.out.println("TAREA-" + iD + " CANCELADA ejecución " + proceso);
    }

    public String getiD() {
        return iD;
    }   

    public Proceso getProceso() {
        return proceso;
    }
    
    @Override
    public int compareTo(HiloEjecucion hilo) {
        Proceso proceso = hilo.getProceso();
        
        return this.proceso.compareTo(proceso);
    }
}
