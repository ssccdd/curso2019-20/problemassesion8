/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo3;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.TIEMPO_PLANIFICACION;

/**
 *
 * @author pedroj
 */
public class Planificador implements Runnable {
    private final String iD;
    private final ExecutorService ejecucion;
    private final BlockingQueue<HiloEjecucion> listaHilos;
    private final List<Future<?>> listaTareas;

    public Planificador(String iD, ExecutorService ejecucion, BlockingQueue<HiloEjecucion> listaHilos, 
                                                              List<Future<?>> listaTareas) {
        this.iD = iD;
        this.ejecucion = ejecucion;
        this.listaHilos = listaHilos;
        this.listaTareas = listaTareas;
    }
    
    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " inicia su ejecución");
        
        try {
            ejecutarHilo();
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " CANCELADA su ejecución");
        }
    }
    
    private void ejecutarHilo() throws InterruptedException {
        // Obtenemos el proceso con mayor prioridad si está disponible
        HiloEjecucion hilo = listaHilos.take();
        
        // Simulamos el tempo de planificación
        TimeUnit.SECONDS.sleep(TIEMPO_PLANIFICACION);
        
        // Se añade al marco de ejecución 
        Future<?> tarea = ejecucion.submit(hilo);
        
        // Se añade a la lista de tareas para una posible cancelación
        listaTareas.add(tarea);
    }

    public String getiD() {
        return iD;
    }
}
