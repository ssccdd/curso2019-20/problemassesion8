/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.ESPERA_FINALIZACION;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.NUCLEOS;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.SISTEMA;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.TIEMPO_CREACION;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.TIEMPO_PLANIFICACION;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService ejecucionSistema;
        ExecutorService ejecucion;
        BlockingQueue<HiloEjecucion> listaHilos;
        BlockingQueue<Proceso> listaFinalizados;
        CountDownLatch esperaFinalizacion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucionSistema = Executors.newScheduledThreadPool(SISTEMA);
        ejecucion = Executors.newFixedThreadPool(NUCLEOS);
        esperaFinalizacion = new CountDownLatch(ESPERA_FINALIZACION);
        listaTareas = new ArrayList();
        
        // Inicialización
        listaHilos = new PriorityBlockingQueue();
        listaFinalizados = new LinkedBlockingDeque();
        
        // Se añade la tarea que creará la ejecución de los procesos periódicamente
        CrearHilo crearHilo = new CrearHilo("CrearHilo",listaHilos, listaFinalizados);
        tarea = ejecucionSistema.scheduleAtFixedRate(crearHilo, INICIO, TIEMPO_CREACION, 
                                                     TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se añade el planificador que se ejecutará periódicamente
        Planificador planificador = new Planificador("Planificador", ejecucion, listaHilos, listaTareas);
        tarea = ejecucionSistema.scheduleAtFixedRate(planificador, INICIO, TIEMPO_PLANIFICACION, 
                                                     TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se crea la tarea de cancelación que se ejecutará pasado el TIEMPO_ESPERA
        // y finalizará las tareas que estén activas
        TareaFinalizacion fin = new TareaFinalizacion(listaTareas,esperaFinalizacion);
        ejecucionSistema.schedule(fin, TIEMPO_ESPERA, TimeUnit.MINUTES);
        System.out.println("HILO(Principal) Espera el tiempo establecido");
        esperaFinalizacion.await();
        
        // Cerramos los marcos de ejecucion
        ejecucionSistema.shutdown();
        ejecucion.shutdown();
        
        // Presentamos los procesos que han finalizado
        System.out.println("HILO(Principal) los procesos finalizados \n______________");
        for(Proceso proceso : listaFinalizados)
            System.out.println(proceso.toString());
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
