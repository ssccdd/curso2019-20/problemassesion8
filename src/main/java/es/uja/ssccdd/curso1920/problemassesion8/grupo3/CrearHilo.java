/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion8.grupo3;

import es.uja.ssccdd.curso1920.problemassesion8.grupo3.Constantes.TipoProceso;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author pedroj
 */
public class CrearHilo implements Runnable {
    private final String iD;
    private final BlockingQueue<HiloEjecucion> listaHilos;
    private final BlockingQueue<Proceso> listaFinalizados;
    
    // Variable de clase
    private static int pID = 0;

    public CrearHilo(String iD, BlockingQueue<HiloEjecucion> listaHilos, BlockingQueue<Proceso> listaFinalizados) {
        this.iD = iD;
        this.listaHilos = listaHilos;
        this.listaFinalizados = listaFinalizados;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " inicia su ejecución");
        TipoProceso tipo = TipoProceso.getProceso();
        
        try {
            
            crearHilo(tipo);
            
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " finaliza su ejecución");
        }
    }

    private void crearHilo(TipoProceso tipo) throws InterruptedException {
        int idProceso = CrearHilo.getPID();
        
        Proceso proceso = new Proceso(idProceso,tipo);
        HiloEjecucion hilo = new HiloEjecucion("Hilo-"+idProceso,proceso, listaFinalizados);
        
        // Lo añadimos a la lista de hilos para su planificación
        listaHilos.put(hilo);
        System.out.println("TAREA-" + iD + " se crea hilo para el proceso " + proceso);
    }
    
    public String getiD() {
        return iD;
    }
    
    /**
     * Simula la generación del identificador del proceso
     * @return 
     */
    private static synchronized int getPID() {
        return ++pID;
    }
}