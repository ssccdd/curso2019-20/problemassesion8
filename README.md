[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 8

Problemas propuestos para la Sesión 8 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

En esta sesión de prácticas se proponen ejercicio para trabajar con el paquete [java.util.concurrent](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/package-frame.html). En los ejercicios nos centraremos en algunas clases que implementan la interface [`BlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/BlockingQueue.html "interface in java.util.concurrent") que permite acceder a los elementos de una estructura de datos de forma segura por los diferentes hilos. Seguimos utilizando la interface [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas. Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2019-20/problemassesion8#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion8#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion8#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion8#grupo-4)

## Grupo 1

En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: ordenar la fabricación de diferentes componentes de un ordenador cada cierto tiempo, el fabricante encargado de ello que almacenará el componente en el almacén y, por último, los proveedores que recogerán los componentes del almacén para completar los ordenadores que componen el pedido final. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas y la clase [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) para las estructuras de datos necesarias. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Componente`: Clase que representa a un componente de un `TipoComponente` para la construcción de un `Ordenador`.
- `Ordenador`: Representa el ordenador que estará compuesto por un `Componente` de los presentes en `TipoComponente`. En otro caso se considera inacabado.
- `TareaFinalizacion`: Esta tarea finaliza la ejecución de una lista de tareas y se sincroniza con el hilo principal para indicar que ha finalizado.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `CrearComponente`: Tarea especializada que ordenará la fabricación de un `TipoComponente`. Para su construcción se necesita un identificador, el **marco de ejecución** para las tareas de fabricación, el almacén donde se guardan los componentes, el `TipoComponente` que ordenará fabricar y una lista donde se almacenará el resultado de la tarea que se ejecuta por si es necesario interrumpirla. 
	- Crea una tarea `Fabricante` para el `TipoComponente` que ordena fabricar.
	- Se almacena el resultado de la ejecución de la tarea por si es necesario interrumpirla.

- `Frabricante`: Simula la fabricación de un componente de ordenador. Para ello será necesario un identificador y un `TipoComponente` y la lista que representa el almacén donde se guarda el componente fabricado.
	- Crea el componente según el `TipoComponente`. 
	- Simula el tiempo de fabricación del componente.
	- Guarda el componente en el almacén.

- `Proveedor`: Tarea que construye ordenadores con las piezas que se encuentran almacenadas. Para su constructor será necesario un identificador, el almacén de las piezas para el ordenador y la lista donde se almacenarán los ordenadores completados por el proveedor.
	- Mientras no se solicite su interrupción:
		- Crea un ordenador:
			- Obtiene cada una de las piezas del ordenador del almacén.
		- Simula el tiempo de fabricación del ordenador.
		- Guarda el ordenador fabricado en la lista de pedidos.

- `Hilo princial`: Realizará los siguientes pasos:
	- Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	- Se crea un `ExecutorService` donde se ejecutarán las tareas de `Fabricacion`. La capacidad que tendrá viene determinada por la constante `TRABAJADORES`.
	- Se crea una tarea `CrearComponente`  por cada `TipoComponente` y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `CREAR_COMPONENTE`. La unidad de tiempo es en segundos.
	- Se crean tantas tareas `Proveedor` como el valor de la constante `PROVEEDORES` y se añaden para su ejecución sin planificación temporal.
	- Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	- El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	- Cierra los marcos de ejecución.
	- Presenta la lista de pedidos antes de finalizar el hilo principal.

## Grupo 2


## Grupo 3

En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: ordenar la creación de los hilos asociados a un proceso, la simulación de ejecución de los procesos mediante hilos y la planificación con una prioridad definida.  Las herramientas que utilizaremos para la solución del ejercicio serán: [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas y las clases [`PriorityBlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/PriorityBlockingQueue.html "class in java.util.concurrent") y [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) para las estructuras de datos necesarias. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Proceso`: Representa al proceso que simulará su ejecución en una unidad de procesamiento.
- `TareaFinalizacion`: Esta tarea finaliza la ejecución de una lista de tareas y se sincroniza con el hilo principal para indicar que ha finalizado.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `CrearHilo`: Ordena la creación de una tarea `HiloEjecucion`. En el constructor será necesario un identificador, la lista con prioridad para almacenar las tareas `HiloEjecucion` y una lista para que el hilo pueda almacenar el proceso que ha finalizado su ejecución
	- Crea un proceso con un `TipoProceso` aleatorio.
	- Asocia ese proceso a una tarea `HiloEjecucion`.
	- Almacena esa tarea en la lista con prioridad.

- `HiloEjecucion`: Esta tarea simula la ejecución de un `TipoProceso` en un hilo del sistema. En su constructor será necesario un identificador, el proceso que simula ejecutar y la lista donde almacenará el proceso una vez finalizada su simulación de ejecución. Esta clase también implementa la interface [`Comparable`](https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html) para establecer la prioridad. Para establecer la prioridad se compararán los procesos que están asociados a la tarea `HiloEjecucion`.
	- Simulamos la ejecución del proceso según su tiempo de ejecución asociado.
	- Almacenamos el proceso en la lista.

- `Planificador`: Simula la planificación con prioridad entre los hilos de ejecución según su proceso asociado. Su constructor necesita un identificador,  el **marco de ejecución** para las tareas `HiloEjecucion`, la lista con prioridad donde se encuentran estas tareas y una lista donde se almacena su resultado de ejecución por si es necesario interrumpirla.
	- Se obtiene la primera tarea de la lista con prioridad.
	- Simulamos un tiempo de planificación según el valor de la constante `TIEMPO_PLANIFICACION`.
	- Añadimos la tarea para su ejecución y guardamos el resultado de su ejecución por si es necesario interrumpirla.

- `Hilo princial`: Realizará los siguientes pasos:
	- Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	- Se crea un `ExecutorService` donde se ejecutarán las tareas de `HiloEjecucion`. La capacidad que tendrá viene determinada por la constante `NUCEOS`.
	- - Se crea una tarea `CrearHilo` y se añade para su ejecución desde el comienzo  y con un periodo de repetición de `TIEMPO_CREACION`. La unidad de tiempo es en segundos.
	- Se crea una tarea `Planificador` y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `TIEMPO_PLANIFICACION`. La unidad de tiempo es en segundos.
	- Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	- El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	- Cierra los marcos de ejecución.
	- Se presenta la lista de los procesos que han finalizado su ejecución.
	- Finaliza el hilo principal.

## Grupo 4

En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: la fabricación de sensores que estarán disponibles según una fecha de fabricación, el promotor que ordenará la instalación de un número de sensores y el instalador que realiza la instalación de ese número de sensores en una lista de casas. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas y las clases [`DelayQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/DelayQueue.html "class in java.util.concurrent") y [`LinkedBlockingDeque`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/LinkedBlockingDeque.html) para las estructuras de datos necesarias. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Sensor`: Simula un tipo de sensor y también dispone de un método que simulará el tiempo necesario para su instalación.
- `Casa`: Simula la casa donde se instalará un sensor. Dispone de una distribución de habitaciones según el `TipoCasa` que viene definido en `Constantes`.
- `TareaFinalizacion`: Esta tarea finaliza la ejecución de una lista de tareas y se sincroniza con el hilo principal para indicar que ha finalizado

El ejercicio consiste en completar la implementación de las siguientes clases:

- `FabricarSensor`: Simula la fabricación de un `TipoSensor` y lo guarda en el almacén. El constructor necesita un identificador y una lista con una disponibilidad temporal de sus elementos.
	- Crea un sensor con `TipoSensor` aleatorio.
	- Se guarda el sensor en el almacén.

- `Promotor`: Ordena la instalación de un número de sensores. En su constructor se necesita un identificador, el **marco de ejecución** para las tareas `Instalador`, el almacén donde se encuentran los sensores con una disponibilidad temporal, una lista donde se almacenan la lista de casas de los instaladores y una lista donde se almacena el resultado de ejecución de las tareas `Instalador` por si es necesario interrumpirlas.
	- Se genera un número de sensores aleatorio de sensores que se instalarán.
	- Se crea la tarea `Instalador`.
	- Se añade la tarea al **marco de ejecución** y se almacena su resultado en la lista por si es necesario interrumpirla.

- `Instalador`: Simula la instalación de un número de sensores almacenados en una lista de casas. El constructor necesita un identificador, el número de sensores que se instalarán, el almacén con los sensores, la lista donde se almacena la lista de casas instaladas.
	- Crear una lista vacía para las casas que se instalarán.
	- Mientras no haya completado la instalación de todos sus sensores:
		- Obtener el primer sensor disponible del almacén.
		- Comprobar si se puede instalar el sensor en alguna casa de la lista.
		- Si no crear una nueva casa donde instalar el sensor y añadirlo a la lista.
	- Guardar la lista de casas instaladas por el instalador a la lista común entre todos los instaladores.

- `Hilo princial`: Realizará los siguientes pasos:
	- Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	- Se crea un `ExecutorService` donde se ejecutarán las tareas de `Instalador`. La capacidad que tendrá viene determinada por la constante `INSTALADORES`.
	- Se crean tantas tareas `FabricarSensor` como indica la constante `FABRICANTES`. Se añaden para su ejecución desde el inicio y con un periodo de repetición de `FABRICACION`. La unidad de tiempo es en segundos.
	- Se crea una tarea `Promotor` y se añade para su ejecución desde el comienzo y con un periodo de repetición de `ESPERA_INSTALADOR`. La unidad de tiempo es en segundos.
	- Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	- El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	- Cierra los marcos de ejecución.
	- Antes de finalizar se muestra la lista con la lista de las casas de cada instalador. Debe quedar claro las casas que pertenecen a cada instalador.
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzIwNDgyNDE5LDE0NjcxMDU5ODldfQ==
-->